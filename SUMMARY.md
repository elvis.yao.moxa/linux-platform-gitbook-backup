# Moxa Linux Platform Gitbook

## Products

* [Platform](Products/Platform/README.md)
    * [x86 Platform](Products/Platform/x86/README.md)
       * Models
          * [DA Series](Products/Platform/x86/DA/README.md)
             * [DA-820C-KL](Products/Platform/x86/DA/DA-820C-KL.md)
             * [DA-682C-KL](Products/Platform/x86/DA/DA-682C-KL.md)
             * [DA-681C-KL](Products/Platform/x86/DA/DA-681C-KL.md)
          * [MC Series](Products/Platform/x86/MC/README.md)
             * [MC-3201-TL](Products/Platform/x86/MC/MC-3201-TL.md)
             * [MC-1200-KL](Products/Platform/x86/MC/MC-1200-KL.md)
             * [MC-1100](Products/Platform/x86/MC/MC-1100.md)
          * [V Series](Products/Platform/x86/V/README.md)
             * [V2403C-KL](Products/Platform/x86/V/V2403C-KL.md)
             * [V2406C-KL](Products/Platform/x86/V/V2406C-KL.md)
          * [EXPC Series](Products/Platform/x86/EXPC/README.md)
             * [EXPC-F2000W](Products/Platform/x86/EXPC/EXPC-F2000W-TG.md)
       * [Linux Distributions](Products/Platform/x86/LinuxDist/README.md)
          * [Debian 11](Products/Platform/x86/LinuxDist/Debian_11.md)
          * [Ubuntu 20.04](Products/Platform/x86/LinuxDist/Ubuntu_20_04.md)
          * [CentOS 7.9](Products/Platform/x86/LinuxDist/CentOS_7_9.md)
       * [Example Code](Products/Platform/x86/ExampleCode/README.md)

    * [32-bits ARM Platform](Products/Platform/arm32/README.md)
    * [64-bits ARM Platform](Products/Platform/arm64/README.md)

## Modules

* RF Modules
    * [WiFi Modules](RF_Modules/WiFi/README.md)
        * [SparkLAN](RF_Modules/WiFi/SparkLAN/README.md)
           * [WPEQ-261ACNI(BT)](RF_Modules/WiFi/SparkLAN/WPEQ-261ACNI_BT.md)
    * [LTE Modules](RF_Modules/LTE/README.md)
        * [Telit](RF_Modules/LTE/Telit/README.md)
           * [LE910C4](RF_Modules/LTE/Telit/LE910C4.md)
