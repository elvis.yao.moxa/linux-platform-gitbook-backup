# model name
## Appearance
- Front view
- Rear view

## Tech Specs
| Computer     | <!-- -->                 |
|--------------|--------------------------|
| CPU          | N/A                      |
| Storage Slot | N/A                      |
| Supported OS | N/A                      |
| Memory Slot  | N/A                      |

| Peripherals          | <!-- -->                 |
|----------------------|--------------------------|
| Ethernet Ports       | N/A                      |
| Serial Ports         | N/A                      |
| Digital Input/Output | N/A                      |
| NMEA                 | N/A                      |
| USB                  | N/A                      |
| Video Output         | N/A                      |
| Audio Input/Output   | N/A                      |
| M.2 Socket           | N/A                      |
| SIM Card Socket      | N/A                      |
| mini PCIe Socket     | N/A                      |

## Models
| <!-- -->      | [model 1]            | [model 2]           |
|---------------|----------------------|---------------------|
| Processor     | N/A                  | N/A                 |
| RAM           | N/A                  | N/A                 |
| HDD/SSD Slots | N/A                  | N/A                 |
| LAN Ports     | N/A                  | N/A                 |
| Serial Ports  | N/A                  | N/A                 |
| DI/DO         | N/A                  | N/A                 |
| NMEA 0183     | N/A                  | N/A                 |
| Power Input   | N/A                  | N/A                 |

## Control Peripherals
N/A
---

### UART
UART modes are controled by 3 gpio pins and it87_serial
- Super IO (IT8786) PIN table

| Device Node | GPIO#0 | GPIO#1 | GPIO#2 | it87_serial |
| ----------  | ------ | ------ | ------ | ----------- |
| /dev/ttyS0  |  GP??  |  GP??  |  GP??  |      ?      |

- GPIO value of UART modes

|   UART mode    | GPIO#0 | GPIO#1 | GPIO#2 | it87_serial |
| -------------- | ------ | ------ | ------ | ----------- |
| RS232          |   1    |    0   |    0   |      0      |
| RS485-2W       |   0    |    1   |    0   |      1      |
| RS422/RS485-4W |   0    |    0   |    1   |      1      |

---

### NMEA
| Port |  Device Node  |
| ---- | ------------  |
|  1   | /dev/ttyUSB0  |
|  2   | /dev/ttyUSB1  |
|  3   | /dev/ttyUSB2  |
|  4   | /dev/ttyUSB3  |

---

### DIO
| DIO index   | GPIO PIN |
| ----------  | -------- |
| DI 0        |    0     |
| DI 1        |    1     |
| DI 2        |    2     |
| DI 3        |    3     |
| DO 0        |    4     |
| DO 1        |    5     |
| DO 2        |    6     |
| DO 3        |    7     |

---

### Cellular
N/A

---

### LAN interface

| LAN Slot |  NIC |  renamed NIC  |
| -------- | ---- | ------------- |
| LAN #?   | eth0 |   enp0s31f6   |

---

### Watchdog