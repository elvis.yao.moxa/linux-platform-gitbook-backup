# Moxa Platforms

## x86 Platform

- DA series
  * [DA-820C-KL](/Products/Platform/x86/DA/DA-820C-KL.md)
  * [DA-682C-KL](/Products/Platform/x86/DA/DA-682C-KL.md)
  * [DA-681C-KL](/Products/Platform/x86/DA/DA-682C-KL.md)

- MC series
  * [MC-1100](/Products/Platform/x86/MC/MC-1100.md)
  * [MC-1200-KL](/Products/Platform/x86/MC/MC-1200-KL.md)
  * [MC-3201-TL](/Products/Platform/x86/MC/MC-3201-TL.md)

- Expansion Module
  * [DA-PRP-HSR-I210](/Products/Platform/x86/ExpansionModule/DA-PRP-HSR-I210.md)
  * [DN-PRP-HSR-I210](/Products/Platform/x86/ExpansionModule/DN-PRP-HSR-I210.md)
  * [DN-FX04](/Products/Platform/x86/ExpansionModule/DN-FX04.md)

- Linux Distributions Support
  * [Debian 11](/Products/Platform/x86/LinuxDist/Debian_11.md)
  * [Ubuntu 20.04](/Products/Platform/x86/LinuxDist/Ubuntu_20_04.md)
  * [CentOS 7.9](/Products/Platform/x86/LinuxDist/CentOS_7_9.md)
