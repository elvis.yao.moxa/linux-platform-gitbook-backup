# Debian 11 (bullseye)
- Kernel version: 5.10.0-10-amd64
- ISO: debian-live-11.2.0-amd64-gnome

## Prerequisite
- Setup network.
- Update system packages to latest version.
    ```sh
    sudo apt update
    ```
- Install building packages.
    ```sh
    sudo apt install git build-essential linux-headers-`uname -r` -y
    ```

## Add intel TCO Watchdog driver to blacklist
Here we use IT8786 chip super I/O as watchdog driver.

> vi /lib/modprobe.d/iTCO-blacklist.conf

```
blacklist iTCO_wdt
blacklist iTCO_vendor_support
```


## Troubleshooting
### 1. Can not load Wi-Fi firmware
- Symptom
    ```sh
    $ dmesg -l err
    [...]
    ath10k_pci 0000:0e:00.0: Failed to find firmware-N.bin (N between 2 and 6) from ath10k/QCA6174/hw3.0: -2
    [...]
    ```
- Solution
    1. Download the latest version **firmware-atheros** from [packages.debian.org](https://packages.debian.org/sid/firmware-atheros), at the bottom of that page you can find the link.
        ```sh
        dpkg -i firmware-atheros_20210818-1_all.deb
        ```
    2. Check driver is loaded.
        ```sh
        dmesg | grep ath10k
        [    6.453056] ath10k_pci 0000:0e:00.0: enabling device (0000 -> 0002)
        [    6.453507] ath10k_pci 0000:0e:00.0: pci irq msi oper_irq_mode 2 irq_mode 0 reset_mode 0
        [    6.732869] ath10k_pci 0000:0e:00.0: firmware: direct-loading firmware ath10k/QCA6174/hw3.0/firmware-6.bin
        [    6.732877] ath10k_pci 0000:0e:00.0: qca6174 hw3.2 target 0x05030000 chip_id 0x00340aff sub 168c:3363
        [    6.732878] ath10k_pci 0000:0e:00.0: kconfig debug 0 debugfs 0 tracing 0 dfs 0 testmode 0
        [    6.733670] ath10k_pci 0000:0e:00.0: firmware ver WLAN.RM.4.4.1-00157-QCARMSWPZ-1 api 6 features wowlan,ignore-otp,mfp crc32 90eebefb
        [    6.837468] ath10k_pci 0000:0e:00.0: firmware: direct-loading firmware ath10k/QCA6174/hw3.0/board-2.bin
        [    6.838190] ath10k_pci 0000:0e:00.0: board_file api 2 bmi_id N/A crc32 318825bf
        [    7.048799] ath10k_pci 0000:0e:00.0: htt-ver 3.60 wmi-op 4 htt-op 3 cal otp max-sta 32 raw 0 hwcrypto 1
        [    7.172671] ath10k_pci 0000:0e:00.0 wlp14s0: renamed from wlan0
        ```
        ```sh
        lspci | grep Atheros
        0e:00.0 Network controller: Qualcomm Atheros QCA6174 802.11ac Wireless Network Adapter (rev 32)
        ```

### 2. Can not load Bluetooth firmware
- Symptom
    ```sh
    $ dmesg -l err
    [...]
    bluetooth hci0: firmware: failed to load qca/rampatch_usb_00000302.bin (-2)
    [...]
    ```
- Solution
    1. Download the latest version **firmware-atheros** from [packages.debian.org](https://packages.debian.org/sid/firmware-atheros), at the bottom of that page you can find the link.
        ```sh
        dpkg -i firmware-atheros_20210818-1_all.deb
        ```
    2. Check driver is loaded.
        ```sh
        dmesg | grep Bluetooth
        [    6.800510] Bluetooth: Core ver 2.22
        [    6.800539] Bluetooth: HCI device and connection manager initialized
        [    6.801608] Bluetooth: HCI socket layer initialized
        [    6.801611] Bluetooth: L2CAP socket layer initialized
        [    6.801617] Bluetooth: SCO socket layer initialized
        [    6.928128] Bluetooth: hci0: using rampatch file: qca/rampatch_usb_00000302.bin
        [    6.928130] Bluetooth: hci0: QCA: patch rome 0x302 build 0x3e8, firmware rome 0x302 build 0x111
        [    7.048278] Bluetooth: hci0: using NVM file: qca/nvm_usb_00000302.bin
        [    7.149267] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
        [    7.149270] Bluetooth: BNEP filters: protocol multicast
        [    7.149274] Bluetooth: BNEP socket layer initialized
        [    8.900651] Bluetooth: RFCOMM TTY layer initialized
        [    8.900657] Bluetooth: RFCOMM socket layer initialized
        [    8.900664] Bluetooth: RFCOMM ver 1.11
        ```
        ```sh
        lsusb | grep Atheros
        Bus 001 Device 004: ID 0cf3:e300 Qualcomm Atheros Communications QCA61x4 Bluetooth 4.0
        ```