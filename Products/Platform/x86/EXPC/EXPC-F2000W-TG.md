# EXPC-F2000W-TG
## Appearance
- Front view
- Rear view

## Tech Specs
| Computer     | <!-- -->                                                             |
|--------------|----------------------------------------------------------------------|
| CPU          | Tiger Lake CPU main Board (Celeron/i3/i5/i7)                         |
| Storage Slot | CFexpress/M.2                                                        |
| Supported OS | Debian-11 (bullseye)<br>Ubuntu-20.04-HWE                             |
| Memory Slot  | 2x DDR4-3200/LPDDR4x-4267, 64GB max                                  |

| Peripherals          | <!-- -->                                                          |
|----------------------|-------------------------------------------------------------------|
| Ethernet Ports       | Auto Sensing 10/100/1000Mbps x 4 (I219 x 1 + I210 x 3)            |
| Serial Ports         | RS-232/422/485 x 3, software selectable (DB9)                     |
| Digital Input/Output | N/A                                                               |
| NMEA                 | N/A                                                               |
| USB                  | USB 2.0 Port x 2 (type-A)<br>USB 3.0 Port x 2 (type-A)            |
| Video Output         | DP++ x 1, VGA x 1                                                 |
| Audio Input/Output   | Line in x 1, Line out x 1, 3.5 mm phone jack                      |
| M.2 Socket           | Cellular 5G module/Storage x 1 (B key)<br>WiFi module x 1 (E key) |
| SIM Card Socket      | micro SIM socket x 1 (push-out type)                              |
| mini PCIe Socket     | N/A                                                               |

## Models
| <!-- -->      | EXPC-F2150W-TG3-DC   | EXPC-F2150W-TG5-DC   | EXPC-F2150W-TG7-DC   | EXPC-F2120W-TG1-DC  | EXPC-F2120W-TG3-DC   |
|---------------|----------------------|----------------------|----------------------|---------------------|----------------------|
| Processor     | Intel Corei3-1115G4E | Intel Corei5-1145GRE | Intel Corei7-1185G7E | Intel Celeron 6305E | Intel Corei3-1115G4E |
| RAM           | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| HDD/SSD Slots | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| LAN Ports     | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| Serial Ports  | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| DI/DO         | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| NMEA 0183     | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |
| Power Input   | N/A                  | N/A                  | N/A                  | N/A                 | N/A                  |

## Control Peripherals
### Prerequisite
Build necessary MOXA out-of-tree kernel module for controlling **Super I/O**.
- Install kernel module tool.
  ```bash
  sudo apt update
  sudo apt install build-essential kmod
  ```
- Install kernel headers.
  ```bash
  sudo apt update
  sudo apt install linux-headers-`uname -r`
  ```
- Build necessary kernel modules.
  - [moxa-it87-gpio-driver](https://github.com/Moxa-Linux/moxa-it87-gpio-driver/tree/master)
  - [moxa-it87-wdt-driver](https://github.com/Moxa-Linux/moxa-it87-wdt-driver)
    - For Ubuntu 20.04/Debian 11 (kernel ver >= 4.19): [moxa-it87-wdt-driver(5.2)](https://github.com/Moxa-Linux/moxa-it87-wdt-driver/tree/5.2/master)
- Install and generate modules.dep.
    ```bash
    make && make install
    depmod -a
    ```
- Example code
  - [mx-pca953x-gpio-ctl](/Products/Platform/x86/ExampleCode/example-code/mx-pca953x-gpio-ctl)
---

### Serial Ports
Serial ports modes are controled by 4 gpio pins from PCA953x chip

- PCA953x PIN table

| UART Port Num | Device Node  | GPIO#0 | GPIO#1 | GPIO#2 | GPIO#3 |
| ------------- | ------------ | ------ | ------ | ------ | ------ |
|       1       | /dev/ttyUSB1 |  GP0   |  GP1   |  GP2   |  GP3   |
|       2       | /dev/ttyUSB2 |  GP4   |  GP5   |  GP6   |  GP7   |
|       3       | /dev/ttyUSB3 |  GP8   |  GP9   |  GP10  |  GP11  |

- GPIO value of UART modes

|   UART mode    | GPIO#0 | GPIO#1 | GPIO#2 | GPIO#3 |
| -------------- | ------ | ------ | ------ | ------ |
| RS232          |   1    |    1   |    0   |    0   |
| RS485-2W       |   0    |    0   |    0   |    1   |
| RS422/RS485-4W |   0    |    0   |    1   |    0   |

- Example for UART Port 1
  - Select Port 1 as **RS232** mode
    ```bash
    mx-pca953x-gpio-ctl set 0 1
    mx-pca953x-gpio-ctl set 1 1
    mx-pca953x-gpio-ctl set 2 0
    mx-pca953x-gpio-ctl set 3 0
    ```
  - Select Port 1 as **RS485-2W** mode
    ```bash
    mx-pca953x-gpio-ctl set 0 0
    mx-pca953x-gpio-ctl set 1 0
    mx-pca953x-gpio-ctl set 2 0
    mx-pca953x-gpio-ctl set 3 1
    ```
  - Select Port 1 as **RS422/RS485-4W** mode
    ```bash
    mx-pca953x-gpio-ctl set 0 0
    mx-pca953x-gpio-ctl set 1 0
    mx-pca953x-gpio-ctl set 2 1
    mx-pca953x-gpio-ctl set 3 0
    ```
  - Get Port 1 mode
    ```bash
    mx-pca953x-gpio-ctl get 0
    mx-pca953x-gpio-ctl get 1
    mx-pca953x-gpio-ctl get 2
    mx-pca953x-gpio-ctl get 3
    ```
  - Result:
    ```bash
    1
    1
    0
    0
    ```
    Then look up the table, port 1 is on **RS232** mode.

- Example for UART Port 2
  - Select Port 2 as **RS232** mode
    ```bash
    mx-pca953x-gpio-ctl set 4 1
    mx-pca953x-gpio-ctl set 5 1
    mx-pca953x-gpio-ctl set 6 0
    mx-pca953x-gpio-ctl set 7 0
    ```
  - Select Port 2 as **RS485-2W** mode
    ```bash
    mx-pca953x-gpio-ctl set 4 0
    mx-pca953x-gpio-ctl set 5 0
    mx-pca953x-gpio-ctl set 6 0
    mx-pca953x-gpio-ctl set 7 1
    ```
  - Select Port 2 as **RS422/RS485-4W** mode
    ```bash
    mx-pca953x-gpio-ctl set 4 0
    mx-pca953x-gpio-ctl set 5 0
    mx-pca953x-gpio-ctl set 6 1
    mx-pca953x-gpio-ctl set 7 0
    ```
  - Get Port 2 mode
    ```bash
    mx-pca953x-gpio-ctl get 4
    mx-pca953x-gpio-ctl get 5
    mx-pca953x-gpio-ctl get 6
    mx-pca953x-gpio-ctl get 7
    ```
  - Result:
    ```text
    1
    1
    0
    0
    ```
    Then look up the table, port 2 is on **RS232** mode.

- Example for UART Port 3
  - Select Port 3 as **RS232** mode
    ```bash
    mx-pca953x-gpio-ctl set 8 1
    mx-pca953x-gpio-ctl set 9 1
    mx-pca953x-gpio-ctl set 10 0
    mx-pca953x-gpio-ctl set 11 0
    ```
  - Select Port 3 as **RS485-2W** mode
    ```bash
    mx-pca953x-gpio-ctl set 8 0
    mx-pca953x-gpio-ctl set 9 0
    mx-pca953x-gpio-ctl set 10 0
    mx-pca953x-gpio-ctl set 11 1
    ```
  - Select Port 3 as **RS422/RS485-4W** mode
    ```bash
    mx-pca953x-gpio-ctl set 8 0
    mx-pca953x-gpio-ctl set 9 0
    mx-pca953x-gpio-ctl set 10 1
    mx-pca953x-gpio-ctl set 11 0
    ```
  - Get Port 3 mode
    ```bash
    mx-pca953x-gpio-ctl get 8
    mx-pca953x-gpio-ctl get 9
    mx-pca953x-gpio-ctl get 10
    mx-pca953x-gpio-ctl get 11
    ```
  - Result:
    ```text
    1
    1
    0
    0
    ```
    Then look up the table, port 3 is on **RS232** mode.

- Use helper script to swtich UART mode.
  - TBD

---

### MCU for Panel Board

TBD

---

### LAN interface

| LAN Slot |  NIC |  renamed NIC  |
| -------- | ---- | ------------- |
| LAN #1   | eth0 |   enp0s31f6   |
| LAN #2   | eth1 |   enp6s0      |
| LAN #3   | eth2 |   enp7s0      |
| LAN #4   | eth3 |   enp8s0      |

- For Ubuntu 20.04 LAN setting:
I219 LAN chip: only support on **Ubuntu 20.04 HWE** kernel version:

```bash
sudo apt update
sudo apt install --install-recommends linux-generic-hwe-20.04
uname -a
Linux moxa 5.11.0-34-generic #36~20.04.1-Ubuntu SMP Fri Aug 27 08:06:32 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux

ip a | grep enp
2: enp6s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
3: enp7s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
4: enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    inet 10.123.8.84/23 brd 10.123.9.255 scope global enp8s0
5: enp0s31f6: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
```
- Ref
https://askubuntu.com/questions/1344156/ubuntu-20-04-2-and-onboard-intel-i219-v

---

### Watchdog
- Example for probing `it87_wdt` driver on boot
  1. Config file `/lib/modprobe.d/watchdog.conf`
      ```text
      # timeout:Watchdog timeout in seconds, default=60 (int)
      # nowayout:Watchdog cannot be stopped once started, default=0 (bool)
      options it87_wdt nowayout=1 timeout=60
      ```

  2. Disable `iTCO_wdt` driver, `/lib/modprobe.d/iTCO-blacklist.conf`
      ```text
      blacklist iTCO_wdt
      blacklist iTCO_vendor_support
      ```

  3. Disalbe `NMI watchdog` driver (for Ubuntu), `/etc/default/grub`
      ```text
      GRUB_CMDLINE_LINUX_DEFAULT="nmi_watchdog=0"
      ```
  4. After adding the kernel parameter, don't forget to update the GRUB
      ```bash
      sudo update-grub
      ```
- Watchdog daemon package (Debian/Ubuntu)
  1. Install package
      ```bash
      sudo apt-get update
      sudo apt-get install watchdog
      ```
  2. Config file `/etc/default/watchdog`
      ```text
      # Start watchdog at boot time? 0 or 1
      run_watchdog=1
      # Start wd_keepalive after stopping watchdog? 0 or 1
      run_wd_keepalive=0
      # Load module before starting watchdog
      watchdog_module="it87_wdt"
      # Specify additional watchdog options here (see manpage).
      ```
  3. To uncomment this to use the watchdog device driver access "file", `/etc/watchdog.conf`
      ```text
      watchdog-device                = /dev/watchdog
      ```

- Example
https://github.com/torvalds/linux/tree/master/tools/testing/selftests/watchdog
