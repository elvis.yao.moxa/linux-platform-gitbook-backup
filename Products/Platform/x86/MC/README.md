# x86 Platform
- MC series
  * [MC-1100](/Products/Platform/x86/MC/MC-1100.md)
  * [MC-1200-KL](/Products/Platform/x86/MC/MC-1200-KL.md)
  * [MC-3201-TL](/Products/Platform/x86/MC/MC-3201-TL.md)
